# Keras
from tensorflow.keras.models import Model
# Scipy
import numpy as np
# OpenCV
import cv2
# Lasternet
from train import model, attention_output, IMG_SHAPE
# PiCamera
from picamera.array import PiRGBArray
from picamera import PiCamera
# CatCarLib
from catcarlib import Car
# General
import time

# initialize the camera and grab a reference to the raw camera capture
camera = PiCamera()
camera.resolution = (128, 128)
camera.framerate = 32
camera.rotation = 180
rawCapture = PiRGBArray(camera, size=(128, 128))

# init the car
cat = Car()

# allow the camera to warmup
time.sleep(0.1)

model.load_weights('/home/pi/Code/Python/Robocat/checkpoint.h5')
sampler = Model(model.input, [model.get_layer('attention_reshape').output, model.get_layer('classifier2').output])
# sampler = Model(model.input, attention_output])

def filter_red(img):
	min_red = min(img[:, :, 2].flatten()[np.argsort(img[:, :, 2].flatten())[-10:]])
	img[img[:, :, 2] < min_red] = 0.
	return img

def find_laser(img):
	img = filter_red(img)
	xs, ys = [i for i in range(img.shape[0])], [i for i in range(img.shape[0])]
	x_weights, y_weights = img[:, :, 2].mean(axis=0)**2, img[:, :, 2].mean(axis=1)**2
	try:
		return int(np.average(xs, weights=x_weights)), int(np.average(ys, weights=y_weights))
	except ZeroDivisionError:
		return 0, 0

def find_max_cords(prediction):
	max_x, max_y, max_val = 0, 0, 0
	for x in range(prediction.shape[0]):
		for y in range(prediction.shape[1]):
			if prediction[x, y] > max_val:
				max_x = x
				max_y = y
	return max_x, max_y

def euclidean(p1, p2):
	return np.sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2)

def move(center):
	if center[0] <= 32:
		cat.left()
		print('Turning left.')
	elif 32 < center[0] < 96:
		cat.back()
		print('Going backwards.')
	else:
		cat.right()
		print('Going right.')

	# time.sleep(0.5)

def method1(x, y, prediction, is_laser_history):
	print('Is Laser (%)', np.mean(is_laser_history))
	if len(is_laser_history) > 10:
		del(is_laser_history[0])
	if not np.mean(is_laser_history) > 0.3:
		cat.stop()
		return
		
	center = find_laser(np.array(x))
	# if center[1] > 64:
	# 	cat.stop()
	# 	return

	# print(euclidean(center, np.where(prediction == np.amax(prediction))))
	if not euclidean(center, np.where(prediction == np.amax(prediction)))[0] < 100:
		cat.stop()
		return
		
	color = (0, 0, 255)
	cv2.circle(y, center, 3, color)
	move(center)

def method2(x, y):
	center = find_laser(np.array(x))
	print('Norm', np.linalg.norm(x, axis=2).max())
	print('BGR', x[center[0], center[1]])
	if np.linalg.norm(x, axis=2).max() > 150:
		color = (255, 0, 0)
		cv2.circle(y, center, 3, color)
		move(center)
	else:
		cat.stop()

frame_history = [np.zeros((IMG_SHAPE[0], IMG_SHAPE[1], 3))]
is_laser_history = []
for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
	try:
		image = np.array(frame.array)
		x = cv2.resize(image, (IMG_SHAPE[0], IMG_SHAPE[1]))
		frame_history.append(np.array(x))
		if len(frame_history) > 10:
			del(frame_history[0])
		x = np.abs(x - np.array(frame_history).mean(axis=0)).astype(np.uint8)
		prediction, is_laser = sampler.predict(np.expand_dims(x, axis=0))
		prediction = prediction.reshape((IMG_SHAPE[0], IMG_SHAPE[1]))
		y = prediction.reshape((IMG_SHAPE[0], IMG_SHAPE[1], 1))
		y = np.uint8(255 * np.concatenate((y, y, y), axis=-1))
		is_laser_history.append(is_laser[0][0])

		method1(x, y, prediction, is_laser_history)
		#(x, y)

		x = x.repeat(2, axis=0).repeat(2, axis=1)
		y = y.repeat(2, axis=0).repeat(2, axis=1)
		# x[:, :, :2] = 0
		# x[x[:, :, 2] <= 200] = 0
		cv2.imshow('Video', np.concatenate((x, y), axis=1))
		cv2.waitKey(20)
		# clear the stream in preparation for the next frame
		rawCapture.truncate(0)
	except KeyboardInterrupt:
		cat.close()
		break