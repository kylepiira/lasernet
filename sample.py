# Keras
from tensorflow.keras.models import Model
from tensorflow.keras.utils import plot_model
# Scipy
import numpy as np
import matplotlib.pyplot as plt
# OpenCV
import cv2
# Lasternet
from train import model, attention_output, IMG_SHAPE

model.load_weights('checkpoint.v2.h5')
plot_model(model, to_file='model.png')

sampler = Model(model.input, [model.get_layer('attention_reshape').output, model.get_layer('classifier2').output])
# sampler = Model(model.input, attention_output])

out1 = cv2.VideoWriter('out1.avi', cv2.VideoWriter_fourcc('M','J','P','G'), 10, (3840, 1080))
laser = cv2.VideoCapture('test.mp4')

def filter_red(img):
	min_red = min(img[:, :, 2].flatten()[np.argsort(img[:, :, 2].flatten())[-10:]])
	img[img[:, :, 2] < min_red] = 0.
	return img

def find_laser(img):
	img = filter_red(img)
	xs, ys = [i for i in range(img.shape[0])], [i for i in range(img.shape[0])]
	x_weights, y_weights = img[:, :, 2].mean(axis=0)**2, img[:, :, 2].mean(axis=1)**2
	return int(np.average(xs, weights=x_weights)), int(np.average(ys, weights=y_weights))

frame_history = [np.zeros((1080, 1920, 3))]
while laser.isOpened():
	# x = cv2.resize(laser.read()[1], (IMG_SHAPE[0], IMG_SHAPE[1]))
	x = np.array(laser.read()[1])
	frame_history.append(np.array(x))
	if len(frame_history) > 10:
		del(frame_history[0])
	y = np.abs(x - np.array(frame_history).mean(axis=0)).astype(np.uint8)
	# prediction, is_laser = sampler.predict(np.expand_dims(x, axis=0))
	# prediction = prediction.reshape((IMG_SHAPE[0], IMG_SHAPE[1]))
	# y = prediction.reshape((IMG_SHAPE[0], IMG_SHAPE[1], 1))
	# y = np.uint8(255 * np.concatenate((y, y, y), axis=-1))
	# if all(is_laser > 0.5):
	# 	center = find_laser(np.array(x))
	# 	color = (0, 0, 255)
	# 	cv2.circle(y, center, 3, color)

	# x = x.repeat(4, axis=0).repeat(4, axis=1)
	# y = y.repeat(4, axis=0).repeat(4, axis=1)
	# x[:, :, :2] = 0
	# x[x[:, :, 2] <= 200] = 0
	out1.write(np.concatenate((x, y), axis=1))
	# cv2.imshow('Video', np.concatenate((x, y), axis=1))
	cv2.waitKey(20)